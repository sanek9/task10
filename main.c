
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define FILE_NAME_SIZE 30
//#define WRITE_RANDOM_DATA

//#define DEBUG

#ifdef DEBUG
#define LOG(str, args...) \
		printf("Debug: ");\
		printf(str, ##args);\
		printf(" \n")
#else
#define LOG(str, args...)
#endif

typedef struct{
	int min;
	int hour;
} Time;


typedef struct{
	int day;
	int mounth;
	int year;
} Date;

typedef struct{
	char *name;
	int size;
	Date crDate;
	Time crTime;
} File;

typedef struct{
	File start;
	File end;
}Range;
int compareNames(File*, File*, int);
int compareSizes(File*, File*, int);
int compareDates(File*, File*, int);
int compareTimes(File*, File*, int);


typedef struct{
	char *name;
	int (*compare_func)(File*, File*, int);
} SI;

typedef struct{
	int idx;
	int decrease;
} SOder;

typedef enum{
	COLUMN_FILE_NAME,
	COLUMN_SIZE,
	COLUMN_CR_DATE,
	COLUMN_CR_TIME,
	COLUMN_length
} Column;

SI ColumnInfo[] = {
	{"File name",	compareNames},
	{"Size",		compareSizes},
	{"Create date",	compareDates},
	{"Create time",	compareTimes}
};

SOder SortOder[] = {{2,0},{0,0},{1,0},{3,0}};

typedef enum{
	InputData_NUMBER,
	InputData_NAME,
	InputData_SIZE,
	InputData_DATE,
	InputData_TIME,
	InputData_FULL_INPUT,
	InputData_RANGE_NAME,
	InputData_RANGE_SIZE,
	InputData_RANGE_DATE,
	InputData_RANGE_TIME,
	InputData_SORT_ODER,
	InputData_length
} InputData;

typedef struct InputDataInfo{
	InputData data;
	char name[32];
	int size;
	int (*parse)(char *, void*, InputData);
	char* msg;

} InputDataInfo;

int parseStr(char*, void*, InputData);
int parseNum(char*, void*, InputData);
int parseName(char*, void*, InputData);
int parseSize(char*, void*, InputData);
int parseDate(char*, void*, InputData);
int parseTime(char*, void*, InputData);
int parseSortOder(char*, void*, InputData);
int parseRange(char*, void*, InputData);
int parseFull(char* src, void* dst, InputData d);


InputDataInfo IDataInfo[InputData_length]={
	{InputData_NUMBER, "number", 9, parseNum, "You must use only one digit"},
	{InputData_NAME, "name", 30, parseName, "You can use any symbols, not more 30."},
	{InputData_SIZE, "size", 9, parseSize, "You must use only one digit"},
	{InputData_DATE, "date", 11,parseDate, "Enter date in format [dd.mm.yyyy]"},
	{InputData_TIME, "time", 5, parseTime, "Enter date in format [mm:hh]"},
	{InputData_FULL_INPUT, "", 60, parseFull, "Input format [File name, size, dd.mm.yyyy hh:mm]"},
	{InputData_NAME, "Name range", 70, parseRange, "Enter range of name (Example A:E)"},
	{InputData_SIZE, "number range", 20, parseRange, "Enter range of size (Example 100:2000)"},
	{InputData_DATE, "date range", 25, parseRange, "Enter range of date (Example 10.08.2001-11.01.2010)"},
	{InputData_TIME, "time range", 15, parseRange, "Enter range of date (Example 10.30-23:15"},
	{InputData_SORT_ODER, "sotr oder", 10, parseSortOder, ""}

};

File ** initFilesArray(int size);
File * getFileInfo(void);
File * getFileRandomInfo(void);
File * getFileRandomInfo2(File** files, int size);
int getInputData(void* val, InputData d, char* msg);
void writeFileInfo(File * file);
void writeFilesData(File ** files, int size);
int selectSort(File ** files, int size);
File ** readDataToArray(File ** files,int* size);
int selectFilesBy(Column col, File** src, int size, File** dst, Range r);
int removeFilesBy(Column col, File** src, int size, File** dst, Range r);
int selectColumn();
void setSortOder();

int main(int argc, char *argv[])
{
	int n = 10, i , s, col;
	Range r;
	srand(time(NULL));
	File ** files;
#ifdef WRITE_RANDOM_DATA
	getInputData(&n,InputData_NUMBER,"Enter size of array");
#endif
	files = initFilesArray(n);
	files = readDataToArray(files, &n);
	writeFilesData(files, n);
	File** sel = initFilesArray(n);
	for(i = 0; i < n; i++){
		sel[i]=files[i];
	}
	s=n;
	while(1){
		getInputData(&i,InputData_NUMBER,"Select operation\
		\n 0 - sort array\n 1 - select element\n 2 - remove element");
		switch(i){
			case 0:
				setSortOder();
				selectSort(sel, s);
				break;
			case 1:
				col = selectColumn();
				getInputData(&r,InputData_RANGE_NAME+col, "Select range");
				s=selectFilesBy(col, sel, s, sel, r);
				break;
			case 2:
				col = selectColumn();
				getInputData(&r,InputData_RANGE_NAME+col, "Select range");
				s=removeFilesBy(col, sel, s, sel, r);
				break;
			case 3:
				for(i = 0; i < n; i++){
					sel[i]=files[i];
				}
				s=n;
				break;
			case 4:
				return 0;
		}
		writeFilesData(sel, s);
		putchar('\n');
	}
	for(i = 0; i < n; i++){
		free(files[i]->name);
		free(files[i]);
	}
	free(files);

	return 0;
}

File ** initFilesArray(int size){
	File ** files;

	files = (File**) calloc(size, sizeof(File*));
	return files;
}

File ** readDataToArray(File ** files, int* size){
	int i;
	for(i = 0;i < *size; i++){
#ifdef WRITE_RANDOM_DATA
		if(i<*size/2)
			files[i] = getFileRandomInfo();
		else
			files[i] = getFileRandomInfo2(files, *size/2);
#else
		files[i] = getFileInfo();
		if(files[i]==NULL){
			break;
		}
		if(i>=*size-1){
			files = (File**)realloc(files, (*size += 10)*sizeof(File*));
		}
#endif
	}
	*size = i;
	files = (File**)realloc(files, *size *sizeof(File*));
	return files;

}


File * getFileInfo(){
	File * file;
	file = (File*) malloc(sizeof(File));
	//file->name = (char *) calloc(FILE_NAME_SIZE, sizeof(char));
#if 0
	getInputData(file,InputData_NAME,"Enter file name");
	getInputData(file, InputData_SIZE, "Enter size of file");
	getInputData(file, InputData_DATE, "Enter create date of file");
	getInputData(file, InputData_TIME, "Enter create time of file");
#else
    if(getInputData(file, InputData_FULL_INPUT, "Enter data")){
		free(file);
		return NULL;
    }
#endif
	return file;
}

void setSortOder(){
	int i;
	printf("Set the sort oder. Enter to set default\n");
	for(i=0; i < COLUMN_length; i++){
		printf("%3d %15s\n",i, ColumnInfo[i].name);
	}
	SOder s;
	for(i=0; i < COLUMN_length; i++){
		getInputData(&s,InputData_SORT_ODER,NULL);
		if(s.idx==COLUMN_length)
			break;
		SortOder[i]=s;
	}
}

int selectColumn(){
	int i;
	//printf("Select column to set range\n");
	for(i=0; i < COLUMN_length; i++){
		printf("%3d %15s\n",i, ColumnInfo[i].name);
	}
	int c;
	getInputData(&c,InputData_NUMBER,"Select column to set range");
	return c;

}
void writeFileInfo(File * file){
	printf("name % 30s size % 8d date %02d-%02d-%04d time %02d:%02d\n", file->name, file->size,
		file->crDate.day, file->crDate.mounth, file->crDate.year,
		file->crTime.hour, file->crTime.min);
}
void writeFilesData(File ** files, int size){
	int i;
	for(i = 0; i < size; i++){
		printf("Idx = %2d ", i);
		writeFileInfo(files[i]);
	}
}
int getDateById(Date * date, int id){
	switch(id){
			case 0:
				return date->year;
			case 1:
				return date->mounth;
			case 2:
				return date->day;
		}
	return 0;
}

int getTimeById(Time * time, int id){
	switch(id){
			case 0:
				return time->hour;
			case 1:
				return time->min;
		}
	return 0;
}

int compareSizes(File *f1, File *f2, int decrease){
	return ((decrease)? -1: 1) * (f1->size - f2->size);
}
int compareNames(File *f1, File *f2, int decrease){
	return ((decrease)? -1: 1) * strcmp(f1->name, f2->name);
}
int compareDates(File *f1, File *f2, int decrease){
	int d1, d2;
	int i;
	for(i = 0; i < 3; i++){
		d1 = getDateById(&f1->crDate, i);
		d2 = getDateById(&f2->crDate, i);
		if(d1!=d2){
			return ((decrease)? -1: 1) * (d1-d2);
		}
	}
	return 0;
}
int compareTimes(File *f1, File *f2, int decrease){
	int t1, t2;
	int i;
	for(i = 0; i < 2; i++){
		t1 = getTimeById(&f1->crTime, i);
		t2 = getTimeById(&f2->crTime, i);
		if(t1!=t2){
			return ((decrease)? -1: 1) * (t1-t2);;
		}
	}
	return 0;
}
int compare(File *file1, File *file2 ){
	int i;
	int res;
	for(i=0;i<4;i++){
		res = ColumnInfo[SortOder[i].idx].compare_func(file1, file2, SortOder[i].decrease);
		if(res)
			break;
	}

	return res;
}
int selectSort(File ** files, int size){
	int i, j, c;
	File * buf;
	for(i = 0; i < size; i++){
		for(j = i+1, c = i; j < size; j++){
			if(compare(files[j], files[c])<0)
				c=j;
		}
		buf = files[i];
		files[i] = files[c];
		files[c] = buf;
	}
    return size;
}

int selectFilesBy(Column col, File** src, int size, File** dst, Range r){
	int i, j;
	for(i=0, j=0; i < size; i++){
		if((ColumnInfo[col].compare_func(src[i], &r.start,0) > 0)&&
			(ColumnInfo[col].compare_func(src[i], &r.end,0) < 0)){
				dst[j]=src[i];
				j++;
			}
	}
	return j;
}
int removeFilesBy(Column col, File** src, int size, File** dst, Range r){
	int i, j;
	for(i=0, j=0; i < size; i++){
		if(!((ColumnInfo[col].compare_func(src[i], &r.start,0) > 0)&&
			(ColumnInfo[col].compare_func(src[i], &r.end,0) < 0))){
				dst[j]=src[i];
				j++;
			}
	}
	return j;
}

char getRandChar(){
	return rand()%(122-97)+97;
}
void setRandomString(char * str, int size){
	int i;
	for(i=0;i<size;i++){
		str[i]=getRandChar();
	}
}
void setRandomTime(Time * time){
	//Date date;
	time->min = rand()%60;
	time->hour = rand()%24;
	//printf("%d %d %d", date->day, date->mounth, date->year);
}
void setRandomDate(Date * date){
	//Date date;
	date->day = rand()%31 + 1;
	date->mounth = rand()%12 + 1;
	date->year = rand()%15 + 2000;
	//printf("%d %d %d", date->day, date->mounth, date->year);
}
File * getFileRandomInfo2(File** files, int size){
	File * file;
	int i;
	file = (File*) malloc(sizeof(File));
	file->name = (char*) calloc(FILE_NAME_SIZE, sizeof(char));
	strcpy(file->name, files[rand()%size]->name);
	i = rand()%size;
	file->crDate.day = files[i]->crDate.day;
	file->crDate.mounth = files[i]->crDate.mounth;
	file->crDate.year = files[i]->crDate.year;
	i = rand()%size;
	file->crTime.hour = files[i]->crTime.hour;
	file->crTime.min = files[i]->crTime.min;
	file->size = files[i]->size;
	return file;

}
File * getFileRandomInfo(){
	File * file;
	file = (File*) malloc(sizeof(File));
	file->name = (char*) calloc(FILE_NAME_SIZE, sizeof(char));
	setRandomString(file->name,7);
	setRandomDate(&file->crDate);
	setRandomTime(&file->crTime);
	file->size = rand()%4096;
	return file;

}

int getInputData(void* val, InputData d, char* msg){
	InputDataInfo idi = IDataInfo[d];
	int b_size = 10;//idi.size;
	char* src_s;
	src_s = (char*) calloc(idi.size,sizeof(char));
	char* e = src_s;
	char buf[b_size];
	int s_size=0;
	int size = idi.size;
	int err = 0;
	int res = 0;
	if(msg)
		printf("%s\n", msg);
	while(1){
		fgets(buf, sizeof(buf),stdin);
		s_size+=strlen(buf);
		if(s_size > size){
			src_s = realloc(src_s, size+idi.size);
			if(!src_s){
				printf("Error realocate memory\n");
				exit(1);
			}
			size+=idi.size;

		}
		strcpy(e, buf);
#ifndef WRITE_RANDOM_DATA
		if(s_size==1){
			res = 1;
			break;
		}
#endif
		e=src_s+s_size;
		if(*(e-1)=='\n'){
			err = -1;
			err = idi.parse(src_s, val, d);
			if (err){
				printf("Invalid input\n%s\n",idi.msg);
				s_size=0;
				e=src_s;
#ifdef __linux__
				__fpurge(stdin);
#else
				fflush(stdin);
#endif // __linux__
				continue;
			}
			break;
		}
	}
	free(src_s);
	return res;

}
int isCorrectDate(Date* date){
	if(date->mounth < 1|| date->mounth > 12)
		return 0;

	int mounth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if(!(date->year%4)&&(date->year%100)|| !(date->year%400))
		mounth[1]++;

	if(date->day < 1 || date->day > mounth[date->mounth-1])
		return 0;
	return 1;
}
int isCorrectTime(Time* time){
	if(time->min < 0 || time->min > 59)
		return 0;
	if(time->hour < 0 || time->hour > 23)
		return 0;
	return 1;
}
int parseStr(char* src, void* dst, InputData d){
	char *e;
	int len;
	while(isspace(*src))
		src++;

	len = strlen(src);
	e=src+len-1;
	while(isspace(*e)){
		e--;
		if(e<src)
			return -1;
	}
	*(e+1)='\0';
	strcpy(dst,src);
	return 0;
}
int parseName(char* src, void* dst, InputData d){
	((File*) dst)->name =(char*) calloc(FILE_NAME_SIZE, sizeof(char));
	return parseStr(src, ((File*) dst)->name , d);
}
int parseNum(char* src, void* dst, InputData d){
	char c;
	if(sscanf(src, "%d %c", (int*) dst, &c)!=1){
		return -1;
	}
	return 0;
}
int parseSize(char* src, void* dst, InputData d){
	return parseNum(src, &((File*) dst)->size , d);
}
int parseDate(char* src, void* dst, InputData d){
	Date* date = &((File*) dst)->crDate;
	char c;
	if(sscanf(src, "%d.%d.%d %c", &date->day, &date->mounth, &date->year, &c)!=3){
		return -1;
	}
	if(!isCorrectDate(date)){
		return -1;
	}
	return 0;
}
int parseTime(char* src, void* dst, InputData d){
	Time* time = &((File*) dst)->crTime;
	char c;
	if(sscanf(src, "%d:%d %c", &time->hour, &time->min, &c)!=2)
		return -1;
	if(!isCorrectTime(time)){
		return -1;
	}

	return 0;

}

int parseSortOder(char* src, void* dst, InputData d){
	char c;
	SOder* s =(SOder*)dst;
	int res = sscanf(src, "%d %d %c", &s->idx, &s->decrease, &c);
	if(res<=0){
		s->idx=COLUMN_length;
		return 0;
	}
	if((res!=2&&res!=1)){
		return -1;
	}
	if(res==1)
		s->decrease=0;
	if(s->idx<0||s->idx>=COLUMN_length)
		return -1;
	return 0;
}

int parseFull(char* src, void* dst, InputData d){
    File* file = (File*)dst;
    char* e;
    char* m;
    e=strstr(src, ", ");
    if(!e)
        return -1;
    *e='\0';
    if(parseName(src, dst, d))
        return -1;
//    printf("1\n");
    m=e+2;

    e=strstr(m, ", ");

    if(!e)
        return -1;
    *e='\0';
    if(parseSize(m, dst, d))
        return -1;
 //   printf("2\n");
    m=e+2;
    e=strstr(m, " ");
    if(!e)
        return -1;
    *e='\0';
    if(parseDate(m, dst, d))
        return -1;
 //   printf("3\n");
 //   printf("%s\n", e+1);
    if(parseTime(e+1, dst, d))
        return -1;
    return 0;


}

int parseRange(char* src, void* dst, InputData d){
	char* s1=src;
	char* s2=src;
	while(*s2!='-'){
		if(*s2=='\0')
			return -1;
		s2++;
	}
	*s2 = '\0';
	s2++;
	Range* r = (Range*) dst;
	if(IDataInfo[IDataInfo[d].data].parse(s1, &r->start, IDataInfo[d].data))
		return -1;
	if(IDataInfo[IDataInfo[d].data].parse(s2, &r->end, IDataInfo[d].data))
		return -1;
	return 0;

}
